;; Author: Chinni Sarat Bhargava
;; Date: 11th March 2018
;; Subject: ch10 of little schemer.

(define build
  (lambda (l1 l2)
    (cons l1 (cons l2 (quote ())))
    )
  )

(define first
  (lambda (lat)
    (car lat)
    ))

(define second
  (lambda (lat)
    (car (cdr lat))
    )
  )

(first (quote ((2 3 8 4 1) (2 7 9))))

(second (quote ((2 3 8 4 1) (2 7 9))))

(build (first (quote ((2 3 8 4 1) (2 7 9))))
       (second (quote ((2 3 8 4 1) (2 7 9)))))

(define new-entry build)

(new-entry '(appetizer entree beverage) '(pate boeuf vin))

(define lookup-in-entry
  (lambda (name entry entry-f)
    (lookup-in-entry-help name
			  (first entry)
			  (second entry)
			  entry-f))
  )

(define lookup-in-entry-help
  (lambda (name names values entry-fn)
    (cond
     ((null? names) (entry-fn name))
     ((eq? (car names) name) (car values))
     (else (lookup-in-entry-help name (cdr names)
				 (cdr values) entry-fn))
     )
    )
  )

(define notpresent-fn
  (lambda (name)
    (display name)
    (display " is not present")
    (newline)
    (display "bye chinni")
    )
  )

(lookup-in-entry 'entree (new-entry '(appetizer entree beverage)
				    '(pate boeuf vin)) notpresent-fn
		 )

(lookup-in-entry 'chinni (new-entry '(appetizer entree beverage)
				    '(pate boeuf vin)) notpresent-fn
		 )

(define extend-table cons)

(define lookup-in-table
  (lambda (name table table-f)
    (cond
     ((null? table) (table-f name))
     (else lookup-in-entry name (first table)
	   (lookup-in-table name (cdr table) table-f))
     )
    )
  )

(lookup-in-table 'entree '(((entree dessert)
			    (spaghetti spumoni))
			   ((appetizer entree beverage)
			    (food tastes good))) notpresent-fn)

;; 
(car  (quote (a b c)))