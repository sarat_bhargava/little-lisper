;; Author: Chinni Sarat Bhargava
;; Date: 25th Feb 2018
;; Very impt syntax is little old be careful!!

(defun rember-f (test? a l)
  "remove a from l"
  (cond
   ((null l) (quote ()))
   ((funcall test? (car l) a) (cdr l))
   (t (cons (car l) (rember-f test? a (cdr l))))
   )
  )

(rember-f (function eq) 'sarat '(chinni sarat bhargava))

(defun eq-c? (x)
  "eq-c?"
  (function 
   (lambda (a)
     (eq a x)
     )))

(setf eq-salad? (eq-c? 'salad))
;; another method.
;; (setf (symbol-function 'eq-salad?) (eq-c? 'salad))

(funcall eq-salad? 'salad)

;; Without using funname
(funcall (eq-c? 'salad) 'salad)

;; now modify rember-f using curying:
(defun rember-f (test?)
  "test?: eq, equal, ="
  (function
   (lambda (a l)
    (cond
      ((null l) (quote ()))
      ((funcall test? (car l) a) (cdr l))
      (t (cons (car l) (funcall (rember-f test?) a (cdr l))))
      )
    )
   )
  )

;;
(setf rember-eq (rember-f (function eq)))

(funcall rember-eq 'sarat '(chinni sarat bhargava))

;; insertL
(defun insertL (new old lat)
  "insert to left"
  (cond
    ((null lat) (quote ()))
    ((eq old (car lat)) (cons new lat))
    (t (cons (car lat) (insertL new old (cdr lat))))
    )
  )

(insertL 'bujji 'chinni '(chinni sarat bhargava))
(insertL 'bujji 'sarat  '(chinni sarat bhargava))

;; insertL-f
(defun insertL-f (test?)
  "insert to left"
  (function 
   (lambda (new old lat)
     (cond
       ((null lat) (quote ()))
       ((funcall test? old (car lat)) (cons new lat))
       (t (cons (car lat) (funcall (insertL-f test?) new old (cdr lat))))
       )
     )
   )
  )

(funcall (insertL-f (function eq)) 'bujji
	 'sarat '(chinni sarat bhargava))

;; insertR-f
(defun insertR-f (test?)
  "insert to right"
  (function 
   (lambda (new old lat)
     (cond
       ((null lat) (quote ()))
       ((funcall test? old (car lat)) (cons old (cons new (cdr lat))))
       (t (cons (car lat) (funcall (insertR-f test?) new old (cdr lat))))
       )
     )
   )
  )

(funcall (insertR-f (function eq)) 'bujji 'sarat '(chinni sarat bhargava))

;; insert-g ; using helper fns
(defun insert-g (left? test? new old lat)
  (cond
    (left? (funcall (insertL-f test?) new old lat))
    (t (funcall (insertR-f test?) new old lat)))
  )

(funcall insert-g t (function eq) new old lat)

;; using currying: ; using helper fns
(defun insert-g (left?)
  (function
   (lambda (test? new old lat)
    (cond
      (left? (funcall (insertL-f test?) new old lat))
      (t (funcall (insertR-f test?) new old lat))
      )
    )
   )
  )

(funcall (insert-g nil) (function eq) 'bujji
	 'sarat '(chinni sarat bhargava))

;; insertg

(defun insertg (which?)
  "insert to right"
  (function 
   (lambda (test? new old lat)
     (cond
       ((null lat) (quote ()))
       ((and  which? (funcall test? old (car lat)))
	(cons old (cons new (cdr lat)))
	)
       ((and  (not which?) (funcall test? old (car lat))) (cons new lat)
	)
       (t (cons (car lat) (funcall (insertg which?)
				   test? new old (cdr lat))))
       )
     )
   )
  )

(funcall (insertg nil) (function eq) 'bujji 'sarat '(chinni sarat bhargava))

;; using helper functions
(defun seqr (new old lat)
  "place new to right"
  (cons old (cons new (cdr lat)))
  )

(defun seql (new old lat)
  "place new to left"
  (cons new lat)
  )

;; using helper functions
(defun seqR (new old lat)
  "place new to right"
  (cons old (cons new lat))
  )

(defun seqL (new old lat)
  "place new to left"
  (cons new (cons old lat))
  )


;; nested functions
(defun insertg (seq)
  (function
   (lambda (test?)
    (function
     (lambda (new old lat)
      (cond
	((null lat) (quote ()))
	((funcall test? old (car lat)) (funcall seq new old (cdr lat)))
	(t (cons (car lat) (funcall (funcall (insertg seq) test?)
				    new old (cdr lat))))
	)
       )
     )
    )
   )
  )

(funcall (funcall (insertg (function seqR)) (function eq))
	 'bujji 'sarat '(chinni sarat bhargava))

;; now define insertR-f, insertL-f using insertg
(setf insertR-f (insertg (function seqR)))
(setf insertL-f (insertg (function seqL)))

(funcall (funcall insertL-f (function eq)) 'bujji
	 'sarat '(chinni sarat bhargava))
(funcall (funcall insertR-f (function eq)) 'bujji
	 'sarat '(chinni sarat bhargava))

;; now define insertR-f, insertL-f using insertg
(setf insertR-f (insertg (function (lambda (new old lat)
			   (cons old (cons new lat))))))
(setf insertL-f (insertg (function (lambda (new old lat)
			   (cons new (cons old lat))))))

(funcall (funcall insertL-f (function eq)) 'bujji
	 'sarat '(chinni sarat bhargava))
(funcall (funcall insertR-f (function eq)) 'bujji
	 'sarat '(chinni sarat bhargava))


;; now define insertR-eq insertL-eq using insertg
(setf insertR-eq (funcall (insertg (function seqR)) (function eq)))
(setf insertL-eq (funcall (insertg (function seqL)) (function eq)))

(funcall insertL-eq 'bujji 'sarat '(chinni sarat bhargava))
(funcall insertR-eq 'bujji 'sarat '(chinni sarat bhargava))

;; insertg
(defun insertg (seq)
  (function
   (lambda (test? new old lat)
    (cond
      ((null lat) (quote ()))
      ((funcall test? old (car lat)) (funcall seq new old (cdr lat)))
      (t (cons (car lat) (funcall (insertg seq) test?
				  new old (cdr lat))))
      )
    )
   )
  )

(funcall (insertg (function seqR)) (function eq)
	 'bujji 'sarat '(chinni sarat bhargava))

;; 
(defun atom-to-function (x)
  "return function + when 'x is seen"
  (cond
    ((eq x 'x) (function *))
    ((eq x '+) (function +))
    )
  )

(atom-to-function 'x)
(atom-to-function '+)

;; operator
(defun operator (nexp)
  "extract operator from nexp"
  (car nexp)
  )

(atom-to-function (operator '(+ 5 6)))

(defun 1st-subexp (aexp)
  (car (cdr aexp))
  )

(defun 2nd-subexp (aexp)
  (car (cdr (cdr aexp)))
  )

;; Now lets use these to modify above value fn:

;; (+ (x 3 6) (+ 8 2))
(setq aexp '(+ (x 3 6) (+ 8 2)))
(defun value (aexp)
  (cond
    ((atom aexp) aexp)
    (t
     (funcall (atom-to-function (operator aexp)) (value (1st-subexp aexp))
	      (value (2nd-subexp aexp)))
     )
    )
  )

(value '(+ (x 3 6) (+ 8 2)))

;; 
(defun multirember (a lat)
  "remove all a's from lat"
  (cond
   ((null lat) (quote ()))
   ((eq (car lat) a) (multirember a (cdr lat)))
   (t (cons (car lat) (multirember a (cdr lat)))
      )
   )
  )
(multirember )

;;
(defun multirember-f (test?)
  "remove all a's from lat"
  (function
   (lambda (a lat)
     (cond
      ((null lat) (quote ()))
      ((funcall test? (car lat) a) (funcall (multirember-f test?) a (cdr lat)))
      (t (cons (car lat) (funcall (multirember-f test?) a (cdr lat)))
	 )
      )
     )
   )
  )

(funcall (multirember-f (function eq)) 'sarat
	 '(chinni sarat bhargava sarat chinni))

(setf multirember-eq? (multirember-f (function eq)))

(funcall multirember-eq? 'sarat
	 '(chinni sarat bhargava sarat chinni))

;;
(defun eq?-tuna (b)
  (eq 'tuna b)
  )

(defun multiremberT (eq?-tuna)
  "remove all a's from lat"
  (function
   (lambda (lat)
     (cond
      ((null lat) (quote ()))
      ((funcall eq?-tuna (car lat)) (funcall (multiremberT eq?-tuna) (cdr lat)))
      (t (cons (car lat) (funcall (multiremberT eq?-tuna) (cdr lat)))
	 )
      )
     )
   )
  )

(funcall (multirembert (function eq?-tuna)) '(shrimp salad tuna salad and tuna))

;; 27th Feb 2018

(defun a-friend (x y)
  (null y)
  )

(defun new-friend (newlat seen)
  (funcall (function a-friend) newlat (cons 'tuna seen))
  )

(defun latest-friend (newlat seen)
  (funcall (function a-friend) (cons 'tuna newlat) seen)
  )

(defun multirember&co (a lat col)
  ""
  (cond
    ((null lat) (funcall col (quote ()) (quote ())))
    ((eq (car lat) a) (multirember&co a (cdr lat)
				      (function new-friend)))
    (t (multirember&co a (cdr lat) (function latest-friend)))
    )
  )

(multirember&co 'tuna '() (function a-friend))

(multirember&co 'tuna '(tuna) (function a-friend))

(multirember&co 'tuna '(and tuna) (function a-friend))