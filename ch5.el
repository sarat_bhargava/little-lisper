;; Author: Chinni Sarat Bhargava
;; Date: 16th Feb 2018

;; rember*
(defun rember* (a lat)
  "remove atom, a from list of atoms lat"
  (cond
   ((null lat) (quote ()))
   ((atom (car lat)) (cond
		      ((eq (car lat) a) (rember* a (cdr lat)))
		      (t (cons (car lat) (rember* a (cdr lat))))
		      ))
   (t (cons (rember* a (car lat)) (rember* a (cdr lat))))
   )
  )


;;;;; occur* ;;;;;;

(defun occur* (a l)
  "occur* "
  (cond
   ((null l) 0)
   ((atom (car l)) (cond
		    ((eq (car l) a) (1+ (occur* a (cdr l))))
		    (t (occur* a (cdr l)))
		    ))
   (t (+ (occur* a (car l))  (occur* a (cdr l)) ))
   )
  )


(setq l '((banana)
		  (split ( ( ( ( banana ice) ) )
			  (crea m ( banana))
			  sherbet))
		  ( banana)
		  (bread)
		  (banana brandy) ))
(occur* 'banana l)

;;;;; subst* ;;;;;;

(defun subst* (new old l)
  "subst*"
  (cond
   ((null l) (quote ()))
   ((atom (car l)) (cond
		    ((eq (car l) old) (cons new
					    (subst* new old (cdr l))))
		    (t (cons (car l) (subst* new old (cdr l))))
		    ))
   (t (cons (subst* new old (car l))  (subst* new old (cdr l)) ))
   )
  )

(subst* 'chinni 'banana l)

;;;;;; insertL* ;;;;;;;

(defun insertL* (new old l)
  "insertL*"
  (cond
   ((null l) (quote ()))
   ((atom (car l)) (cond
		    ((eq (car l) old) (cons new
					    (cons old (insertL* new old (cdr l)))))
		    (t (cons (car l) (insertL* new old (cdr l))))
		    ))
   (t (cons (cons new (cons old (insertL* new old (car l))))
	    (cons new (cons old (insertL* new old (cdr l))))))
   )
  )

(insertL* 'chinni 'banana l)

;;;;;;; member* ;;;;;;

(defun member* (a l)
  "member* "
  (cond
   ((null l) nil)
   ((atom (car l)) (cond
		    ((eq (car l) a) t)
		    (t (member* a (cdr l)))
		    ))
   (t (or (member* a (car l))  (member* a (cdr l)) ))
   )
  )

(member* 'sarat l)

;;;;;;;;;; eqlist ;;;;;;

(defun eqlist (l1 l2)
  (cond
   ((and (null l2) (null l1)) t)
   ((and (null l1) (not (null l2))) nil)
   ((and (null l2) (not (null l1))) nil)
   (t
    (cond
     ((and (atom (car l1)) (atom (car l2)))
      (cond
      ((eq (car l1) (car l2)) (eqlist (cdr l1) (cdr l2)))
      (t nil))
      )
     ((or (and (atom (car l1)) (not (atom (car l2))))
	  (and (atom (car l2)) (not (atom (car l1)))))
      nil)
    (t
     (and (eqlist (car l1) (car l2)) (eqlist (cdr l1) (cdr l2)))
     )
   )
   )
  )
)

(eqlist '(beef ( (sausage)) (and (soda)))
	'(beef ( (sausage) ) (and (soda ) ) ))

(eqlist '(banana ( (split) ) )
	'( ( banana) (split)) )

;; write the same using eqan
;; first let's write eqan

(defun eqan (a1 a2)
  "compares 2 atoms and gives t/nil"
  (cond
   (and (numberp a1) (numberp a2)
	(= a1 a2)
	)
   (or (numberp a1) (numberp a2)
       nil)
   (else
    (eq a1 a2)
    )
   )
  )

;; eqlist defined in little lisper book
(defun eqlist (l1 l2)
  (cond ;; conds written like truth table
   ((and (null l1) (null l2)) t)
   ((and (null l1) (atom (car l2))) nil)
   ((null l1) nil)
   ((and (atom (car l1)) (null l2)) nil)
   ((and (atom (car l1)) (atom (car l2)))
    (and (eqan (car l1) (car l2)) (eqlist (cdr l1) (cdr l2))))
   (atom (car l1) nil)
   ((null l2) nil)
   ((atom (car l2)) nil)
   (t
    (and (eqlist (car l1) (car l2)) (eqlist (cdr l1) (cdr l2)))
    )
   )
  )

;; improved way of writing eglist
;; clubbing the conds
(defun eqlist (l1 l2)
  (cond ;; conds written like truth table
   ((and (null l1) (null l2)) t)
   ((or (null l1) (null l2)) nil)
   ((and (atom (car l1)) (atom (car l2)))
    (and (eqan (car l1) (car l2)) (eqlist (cdr l1) (cdr l2))))
   ((or (atom (car l1)) (atom (car l2))) nil)
   (t
    (and (eqlist (car l1) (car l2)) (eqlist (cdr l1) (cdr l2)))
    )
   )
  )


;;;;;;; equal ;;;;;
(defun equal (s1 s2)
  (cond
   ((and (atom s1) (atom s2)) (eqan s1 s2))
   ((atom s1) nil)
   ((atom s2) nil)
   (t
    (eqlist s1 s2)
    )
   )
)

;;;;; eqlist using equal
(defun eqlist (l1 l2)
  (cond
   ((and (null l1) (null l2)) t)
   ((or (null l1) (null l2)) nil)
   (t (and (equal (car l1) (car l2)) (equal (cdr l1) (cdr l2))))
   )
  )

