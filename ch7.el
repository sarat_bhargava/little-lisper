;; Author: Chinni Sarat Bhargava
;; Date: 24th Feb 2018

(defun member (a lat)
  (cond
   ((null lat) nil)
   ((eq a (car lat)) t)
   (t (member a (cdr lat)))
   )
  )

(defun set (lat)
  "is lat a set?"
  (cond
   ((null lat) t)
   ((member (car lat) (cdr lat)) nil)
   (t (set (cdr lat)))
   )
  )

(setq lat '(apples banana apple orange grapes))

(set lat)

;; remove member
(defun rember (a lat)
  "remove atom, a from list of atoms lat"
  (cond
   ((null lat) (quote ()))
   ((eq (car lat) a) (cdr lat))
   (t (cons (car lat) (rember a (cdr lat))))
   )
  )


;; makeset  
(defun makeset (lat)
  "create a set out of given lat"
  (cond
   ((null lat) nil)
   ((member (car lat) (cdr lat)) (makeset
				  (cons (car lat) (rember (car lat) (cdr lat)))))
   (t (cons (car lat) (makeset (cdr lat)))))
  )

(setq lat '(apple peach pear peach
		  plum apple lemon peach))

(makeset lat)

;; subset
(defun subset (set1 set2)
  "is set1 subset of set2"
  (cond
   ((null set1) t)
   ((member (car set1) set2) (subset (cdr set1) set2))
   (t nil)
   )
  )

;; subset using and
(defun subset (set1 set2)
  "is set1 subset of set2"
  (cond
   ((null set1) t)
   (t (and (member (car set1) set2) (subset (cdr set1) set2)))
   )
  )

(subset'(4 pounds of horseradish)
       '(four pounds chicken and
	      5 ounces horseradish))

(subset '(5 chicken wings) '(5 hamburgers 2 pieces fried chicken and
			       light duckling wings))

;; eqset
(defun eqset (set1 set2)
  "function eqset"
  (and (subset set1 set2) (subset set2 set1))
  )

;; intersect
(defun intersect? (set1 set2)
  "does set1 and set2 intersect?"
  (cond
   ((null set1) t)
   ((member (car set1) set2) t)
   (t (intersect (cdr set1) set2))
   )
  )

;; intesect using or
(defun intersect? (set1 set2)
  "does set1 and set2 intersect?"
  (cond
   ((null set1) t)
   (or (member (car set1) set2) (intersect (cdr set1) set2))
   )
  )

;; intesect
(defun intersect (set1 set2)
  "what is intesection of set1 and set2?"
  (cond
   ((null set1) '())
   ((member (car set1) set2) (cons
		 (car set1) (intersect (cdr set1) set2)))
   (t (intersect (cdr set1) set2))
   )
  )

(intersect '(stewed tomatoes and macaroni) '(macaroni and cheese))

;; union
(defun union (set1 set2)
  "what is union of set1 and set2?"
  (cond
   ((null set1) set2)
   ((member (car set1) set2) (union (cdr set1) set2))
   (t (cons (car set1) (union (cdr set1) set2)))
   )
  )

;; intersectall
(defun intersectall (l-set)
  "intersection of all given sets"
  (cond
   ((null (cdr l-set)) (car l-set))
   ((intersectall (cons (intersect (car l-set) (car (cdr l-set))) (cdr (cdr l-set)))))
   )
  )

(setq l-set '((6 pears and) (3 peaches and 6 peppers)
       (8 pears and 6 plums) (and 6 prunes with some apples)))

(intersectall l-set)

;; intersectall simplified way using recursion in better way
(defun intersectall (l-set)
  "intersection of all given sets"
  (cond
   ((null (cdr l-set)) (car l-set))
   (t (intersect (car l-set)
		 (intersectall (cdr l-set))))
   )
  )

;; a-pair
(defun a-pair (l)
  "is l a pair?"
   (and (car l) (car (cdr l)) (not (car (cdr (cdr l)))))
)

(a-pair '(full (house)))
(a-pair '(chinni sarat))
(a-pair '(full))

(defun a-pair (l)
  "is l a pair?"
  (cond
   ((atom l) nil)
   ((null l) nil)
   ((null (cdr l)) nil)
   ((null (cdr (cdr l))) t)
   (t nil)
   )
  )

(defun firsts (lat)
  "first element of each list of list"
  (cond
   ((null lat) (quote ()))
   (t (cons (car (car lat)) (firsts (cdr lat))))
   )
  )

(defun seconds (lat)
  "second element of each list of list"
  (cond
   ((null lat) (quote ()))
   (t (cons (car (cdr (car lat))) (seconds (cdr lat))))
   )
  )

(firsts '((d 4) (b 0) (b 9) (e 5) (g 4) ))

(defun fun (rel)
  "is the given relation a function"
  (set (firsts rel))
  )

(fun '((d 4) (b 0) (b 9) (e 5) (g 4)))
(fun '((d 4) (b 0) (z 9) (e 5) (g 4)))

;; revrel
(defun revrel (rel)
  ""
  (cond
   ((null rel) (quote ()))
   (t (cons (cons (car (cdr (car rel))) (cons (car (car rel)) '())) (revrel (cdr rel))))
   )
  )

(revrel '((d 4) (b 0) (z 9) (e 5) (g 4)))

;; help functions
(defun first (p)
  "first element of a pair
  (car p)"
  )

(defun second (p)
  "second element of a pair"
  (car (cdr p))
  )

(defun build (f s)
  "build pair using first and second element"
  (cons f (cons s '()))
  )

;; revrel using helper functions
(defun revrel (rel)
  "reverse the relations"
  (cond
   ((null rel) (quote ()))
   (t (cons (build (second (car rel)) (first (car rel)))
	    (revrel (cdr rel))))
   )
  )
(revrel '((d 4) (b 0)))


;; revpair
(defun revpair (pair)
  "reverse the pair"
  (build (second pair) (first pair))
  )

;; revrel
(defun revrel (rel)
  ""
  (cond
   ((null rel) (quote ()))
   (t (cons (revpair (car rel)) (revrel (cdr rel))))
   )
  )

(revrel '((d 4) (b 0)))

;; fullfun 
(defun fullfun (fun)
  "is given fun a one to one function"
  (set (seconds fun))
  )

(setq fun '( (grapes raisin)
	     (plum prune)
	     (stewed grape)))
(fullfun fun)
(setq fun '( (grapes raisin)
	     (plum prune)
	     (stewed prune)))
(fullfun fun)


