;; Author: Chinni Sarat Bhargava
;; Date: 2nd March 2018
;; 

(defun looking (a lat)
  "recursives searches for a lat starting from (car lat)
   and visiting (car lat) position of lat"
  (keeplooking a (pick 1 lat) lat)
  )

(defun keeplooking (a sorn lat)
  (cond
   ((and (not (numberp sorn)) (eq sorn a)) t)
   (t (keeplooking a (pick sorn lat) lat))
   )
  )

(defun pick (num lat)
  "pick element at pos num from lat"
  (cond
   ((= (1- num) 0) (car lat))
   (t (pick (1- num) (cdr lat)))
   )
  )

(keeplooking (quote sarat) 3 '(3 5 6 sarat 5 4 3))

(setq lat '(7 2 4 7 5 6 3))
(keeplooking (quote sarat) (pick 1 lat) lat)

(setq lat '(3 5 6 sarat 5 4 3))
(looking 'sarat lat)

;; helper functions
(defun first (L)
  (car L)
  )

(defun second (L)
  (car (cdr L))
  )

(defun build (l1 l2)
  (cons l1 (cons l2 '()))
  )

(defun shift (L)
  (build (first (first L)) (build (second (first L)) (second L)))
  )

(shift '((a b) c))
(shift '((a b) (c d)))

(define align
  (lambda (pora)
    (cond
     ((atom? pora) pora)
     ((a-pair? (first pora))
      (align (shift pora)))
     (else (build (first pora)
		  (align (second pora))))))
  )