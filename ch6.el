;; Author: Chinni Sarat Bhargava
;; Date: 21st Feb 2018

;; works for (2 + 3)
(defun numbered (aexp)
  (cond
   ((atom aexp) (numberp aexp))
   (t (and (numberp (car aexp))
	   (numberp (car (cdr (cdr aexp))))))
   )
  )
(numbered (quote (2 + 3)))

;; works for (2 + 3 + 7) also
(defun numbered (aexp)
  (cond
   ((atom aexp) (numberp aexp))
   ((null (cdr (cdr (cdr aexp)))) (and (numberp (car aexp))
				       (numberp (car (cdr (cdr aexp))))))
   (t (and (numberp (car aexp)) (numbered (cdr (cdr aexp))))
      )
   )
  )
(numbered (quote (2 + 3 + 4 + 5)))

;; works for (2 + (3 + 9))
;; works fine!!!
(defun numbered (aexp)
  (cond
   ((atom aexp) (numberp aexp))
   (t (and (numbered (car aexp))
	   (numbered (car (cdr (cdr aexp))))))
   )
  )
(numbered (quote (2 + (3 + 5))))
(numbered (quote (2 + (3 + ((5 + 2) + 2)))))


;; value
(defun value (aexp)
  (cond
   ((atom aexp) aexp)
   ((eq (car (cdr aexp)) (quote +))
    (+ (value (car aexp)) (value (car (cdr (cdr aexp))))))
   ((eq (car (cdr aexp)) (quote x))
    (* (value (car aexp)) (value (car (cdr (cdr aexp))))))
   )
  )

(value (quote (2 + (3 + 8))))

;; (+ (x 3 6) (+ 8 2))
(defun value (aexp)
  (cond
   ((atom aexp) aexp)
   ((eq (car aexp) (quote +))
    (+ (value (car (cdr aexp))) (value (car (cdr (cdr aexp))))))
   ((eq (car aexp) (quote x))
    (* (value (car (cdr aexp))) (value (car (cdr (cdr aexp))))))
   )
  )

;; helper functions
(defun  operator (aexp)
  (car aexp)
  )

(defun 1st-subexp (aexp)
  (car (cdr aexp))
  )

(defun 2nd-subexp (aexp)
  (car (cdr (cdr aexp)))
  )

;; Now lets use these to modify above value fn:

;; (+ (x 3 6) (+ 8 2))
(defun value (aexp)
  (cond
   ((atom aexp) aexp)
   ((eq (operator aexp) (quote +))
    (+ (value (1st-subexp aexp)) (value (2nd-subexp aexp))))
   ((eq (car aexp) (quote x))
    (* (value (1st-subexp aexp)) (value (2nd-subexp aexp))))
   )
  )

;; now we can change helper functions to work value
;; function (2 + 3) instead of (+ 2 3) style.

(value '(+ (x 3 6) (+ 8 2)))
(value '(+ (x 2 3) 3))

(+ (() ()) (() () ()))

(addn (a b))

(defun addn (a b)
  "New add function:"
  (cond
   ((null b) a)
   (t (cons '() (addn a (cdr b))))
   )
  )

(addn '(() () () () ())  '(() ()) )
