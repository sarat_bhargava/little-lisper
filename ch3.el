;; Author: Chinni Sarat Bhargava
;; Date: 20th Feb 2018

;; remove member
(defun rember (a lat)
  "remove atom, a from list of atoms lat"
  (cond
   ((null lat) (quote ()))
   ((eq (car lat) a) (cdr lat))
   (t (cons (car lat) (rember a (cdr lat))))
   )
  )

(setq lat '(coffee cup tea cup and hick cup))
(setq a 'cup)

(rember a lat)

;; gather first elements 
(defun firsts (l)
  "extract first atom from each list of list"
  (cond
   ((null l) (quote ()))
   (t (cons (car (car l)) (firsts (cdr l)))
      )
   )
  )

(setq l '( (five plu ms)
	   (four)
	   (eleven green oranges)))

(firsts l)

;; insertR
(defun insertR (new old lat)
  "insert atom right of the old atom."
  (cond
   ((null lat) (quote ()))
   ((eq (car lat) old) (cons (car lat)
			     (cons new (cdr lat))))
   (t (cons (car lat) (insertR new old (cdr lat))))
   )
  )

(setq lat '(ice cream with fudge for dessert))
(setq new 'topping)
(setq old 'fudge)
(insertR new old lat)

;; insertL
(defun insertL (new old lat)
  "insert atom left of the old atom."
  (cond
   ((null lat) (quote ()))
   ((eq (car lat) old) (cons new lat))
   (t (cons (car lat) (insertL new old (cdr lat))))
   )
  )

(setq lat '(ice cream with fudge for dessert))
(setq new 'topping)
(setq old 'fudge)
(insertL new old lat)

;; subst
(defun subst (new old lat)
  "subst old atom with new atom"
  (cond
   ((null lat) (quote ()))
   ((eq (car lat) old) (cons new (cdr lat)))
   (t (cons (car lat) (subst new old (cdr lat))))
   )
  )

(setq lat '(ice cream with fudge for dessert))
(setq new 'topping)
(setq old 'fudge)
(subst new old lat)

;; subst2
(defun subst2 (new old1 old2 lat)
  "subst2 old atom with new atom"
  (cond
   ((null lat) (quote ()))
   ((eq (car lat) old1) (cons new (cdr lat)))
   ((eq (car lat) old2) (cons new (cdr lat)))
   (t (cons (car lat) (subst2 new old1 old2 (cdr lat))))
   )
  )

(setq lat '(ice cream with fudge for dessert))
(setq new 'topping)
(setq old1 'fudge)
(setq old2 'cream)
(subst2 new old1 old2 lat)

;; multirember
(defun multirember (a lat)
  "remove all a's from lat"
  (cond
   ((null lat) (quote ()))
   ((eq (car lat) a) (multirember a (cdr lat)))
   (t (cons (car lat) (multirember a (cdr lat)))
      )
   )
  )

(setq lat '(coffee cup tea cup and hick cup))
(setq a 'cup)
(multirember a lat)

;; multiinsertR
(defun multiinsertR (new old lat)
  "insert atom right of the old atom."
  (cond
   ((null lat) (quote ()))
   ((eq (car lat) old) (cons (car lat)
			     (cons new (multiinsertR
					new old (cdr lat)))))
   (t (cons (car lat) (multiinsertR new old (cdr lat))))
   )
  )

(setq lat '(ice fudge cream with fudge for dessert fudge))
(setq new 'topping)
(setq old 'fudge)
(multiinsertR new old lat)

;; multiinsertL
(defun multiinsertL (new old lat)
  "insert atom left of the old atom."
  (cond
   ((null lat) (quote ()))
   ((eq (car lat) old) (cons new (
				  cons old (multiinsertL
					    new old (cdr lat)))))
   (t (cons (car lat) (multiinsertL new old (cdr lat))))
   )
  )

(setq lat '(ice fudge cream with fudge for dessert fudge))
(setq new 'topping)
(setq old 'fudge)
(multiinsertL new old lat)

;; multisubst
(defun multisubst (new old lat)
  "subst old atom with new atom"
  (cond
   ((null lat) (quote ()))
   ((eq (car lat) old) (cons new (multisubst new old (cdr lat))))
   (t (cons (car lat) (multisubst new old (cdr lat))))
   )
  )

(setq lat '(ice fudge cream with fudge for dessert fudge))
(setq new 'topping)
(setq old 'fudge)
(multisubst new old lat)